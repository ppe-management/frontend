import { configure, extend, ValidationObserver, ValidationProvider } from 'vee-validate'
import { required, mimes } from 'vee-validate/dist/rules';
import i18n from '@/i18n'

configure(
  defaultMessage: (_, values) => i18n.t("validations.#{values._rule_}", values)
)

extend('uniqueness', 
  validate: (value, args) ->
    collection = args.collection
    if args.case_sensitive? and not args.case_sensitive
      value = value.toLowerCase()
      collection = (item.toLowerCase() for item in collection)
    collection.indexOf(value) == -1
  params: ['collection', 'case_sensitive']
)

extend('required', required)
extend('mimes', mimes)
