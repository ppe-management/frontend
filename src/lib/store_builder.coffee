export default (object, property, component, build) ->
  object[property] = build(component)
  component.$store.watch(
    (state, getters) => build(getters)
    () => object[property] = build(component)
  )
