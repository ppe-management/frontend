export default (data, filename) ->
  url = window.URL.createObjectURL(new Blob([data]))
  link = document.createElement('a')
  link.href = url
  link.download = filename
  document.body.appendChild(link)
  link.click()
