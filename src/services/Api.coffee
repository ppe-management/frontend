import axios from 'axios'

baseURL = 'http://localhost:3000'

export default
  baseURL: baseURL
  axios: axios.create(
      baseURL: baseURL
      withCredentials: false
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
    )
  login: (email, token) ->
    this.axios.defaults.headers.common['X-User-Email'] = email
    this.axios.defaults.headers.common['X-User-Token'] = token
  logout: () ->
    delete this.axios.defaults.headers.common['X-User-Email']
    delete this.axios.defaults.headers.common['X-User-Token']
