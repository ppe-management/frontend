import Api from '@/services/Api'

export default
  login: (params) ->
    Api.axios.post('/users/sign_in/', params)
  logout: (params) ->
    localStorage.removeItem('email')
    localStorage.removeItem('token')

  listEquipmentCategories: () ->
    Api.axios.get('/equipment/categories/')
  getEquipmentCategories: (id) ->
    Api.axios.get('/equipment/categories/' + id)
  deleteEquipmentCategories: (id) ->
    Api.axios.delete('/equipment/categories/' + id)
  createEquipmentCategories: (params) ->
    Api.axios.post('/equipment/categories/', params)
  editEquipmentCategories: (params) ->
    Api.axios.put('/equipment/categories/' + params.id, params)

  listEquipment: () ->
    Api.axios.get('/equipment/')
  exportEquipment: () ->
    Api.axios.get('/equipment.zip', {responseType: 'blob'})
  getEquipment: (id) ->
    Api.axios.get('/equipment/' + id)
  deleteEquipment: (id) ->
    Api.axios.delete('/equipment/' + id)
  createEquipment: (params) ->
    Api.axios.post('/equipment/', params)
  editEquipment: (params) ->
    Api.axios.put('/equipment/' + params.id, params)

  listEquipmentBatchParts: (equipment_id) ->
    Api.axios.get('/equipment/' + equipment_id + '/batch_parts/')
  getEquipmentBatchParts: (equipment_id, id) ->
    Api.axios.get('/equipment/' + equipment_id + '/batch_parts/'  + id)
  deleteEquipmentBatchParts: (equipment_id, id) ->
    Api.axios.delete('/equipment/' + equipment_id + '/batch_parts/'  + id)
  createEquipmentBatchParts: (equipment_id, params) ->
    Api.axios.post('/equipment/' + equipment_id + '/batch_parts/', params)
  editEquipmentBatchParts: (equipment_id, params) ->
    Api.axios.put('/equipment/' + equipment_id + '/batch_parts/' + params.id, params)

  listInspections: (equipment_id) ->
    Api.axios.get('/equipment/' + equipment_id + '/inspections/')
  getInspections: (equipment_id, id) ->
    Api.axios.get('/equipment/' + equipment_id + '/inspections/'  + id)
  deleteInspections: (equipment_id, id) ->
    Api.axios.delete('/equipment/' + equipment_id + '/inspections/'  + id)
  createInspections: (equipment_id, params) ->
    Api.axios.post('/equipment/' + equipment_id + '/inspections/', params)
  editInspections: (equipment_id, params) ->
    Api.axios.put('/equipment/' + equipment_id + '/inspections/' + params.id, params)

  listInspectionsBatchParts: (equipment_id, inspection_id) ->
    Api.axios.get('/equipment/' + equipment_id + '/inspections/' + inspection_id + '/batch_parts/')
  getEInspectionsBatchParts: (equipment_id, inspection_id, id) ->
    Api.axios.get('/equipment/' + equipment_id + '/inspections/' + inspection_id + '/batch_parts/'  + id)
  deleteInspectionsBatchParts: (equipment_id, inspection_id, id) ->
    Api.axios.delete('/equipment/' + equipment_id + '/inspections/' + inspection_id + '/batch_parts/'  + id)
  createInspectionsBatchParts: (equipment_id, inspection_id, params) ->
    Api.axios.post('/equipment/' + equipment_id + '/inspections/' + inspection_id + '/batch_parts/', params)
  editInspectionsBatchParts: (equipment_id, inspection_id, params) ->
    Api.axios.put('/equipment/' + equipment_id + '/inspections/' + inspection_id + '/batch_parts/' + params.id, params)

  listAttachmentsCategories: () ->
    Api.axios.get('/attachments/categories/')
  getAttachmentsCategories: (id) ->
    Api.axios.get('/attachments/categories/' + id)
  deleteAttachmentsCategories: (id) ->
    Api.axios.delete('/attachments/categories/' + id)
  createAttachmentsCategories: (params) ->
    Api.axios.post('/attachments/categories/', params)
  editAttachmentsCategories: (params) ->
    Api.axios.put('/attachments/categories/' + params.id, params)

  listAttachments: () ->
    Api.axios.get('/attachments/')
  getAttachments: (id) ->
    Api.axios.get('/attachments/' + id)
  deleteAttachments: (id) ->
    Api.axios.delete('/attachments/' + id)
  createAttachments: (params) ->
    data = new FormData()
    for key in Object.keys(params)
      data.append('attachment[' + key + ']', params[key])
    Api.axios.post('/attachments/', data)
  editAttachments: (params) ->
    Api.axios.put('/attachments/' + params.id, params)

  listEquipmentAttachments: (equipment_id) ->
    Api.axios.get('/equipment/' + equipment_id + '/attachments/')
  removeEquipmentAttachments: (equipment_id, id) ->
    Api.axios.delete('/equipment/' + equipment_id + '/attachments/'  + id)
  addEquipmentAttachments: (equipment_id, id) ->
    Api.axios.post('/equipment/' + equipment_id + '/attachments/' + id)
