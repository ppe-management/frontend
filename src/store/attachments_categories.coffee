import PPE from '@/services/PPE'

export default
  state: {}
  getters:
    newAttachmentsCategory: (state) =>
      () ->
        name: ''
    getAttachmentsCategory: (state, getters, rootState) =>
      (id) ->
        id = parseInt(id)
        rootState.attachments_categories.find((category) => category.id == id)
    editAttachmentsCategory: (state, getters, rootState) =>
      (id) ->
        state_category = getters.getAttachmentsCategory(id)
        return getters.newAttachmentsCategory() if not state_category
        category = { ...state_category }
        delete category.attachments
        category
  mutations: {}
  actions:
    createAttachmentsCategory: (context, category) ->
      category.id = (await PPE.createAttachmentsCategories(category)).data.id
      category.attachments = []
      context.commit('addAttachmentsCategory', category)

    updateAttachmentsCategory: (context, category) ->
      (await PPE.editAttachmentsCategories(category)).data
      category.attachments = context.getters.getAttachmentsCategory(category.id).attachments
      context.commit('deleteAttachmentsCategory', category)
      context.commit('addAttachmentsCategory', category)

    deleteAttachmentsCategory: (context, category) ->
      return if category.attachments.length > 0
      await PPE.deleteAttachmentsCategories(category.id)
      context.commit('deleteAttachmentsCategory', category)
