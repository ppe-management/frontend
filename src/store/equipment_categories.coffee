import PPE from '@/services/PPE'

export default
  state: {}
  getters:
    newEquipmentCategory: (state) =>
      () ->
        name: ''
    getEquipmentCategory: (state, getters, rootState) =>
      (id) ->
        id = parseInt(id)
        rootState.equipment_categories.find((category) => category.id == id)
    editEquipmentCategory: (state, getters, rootState) =>
      (id) ->
        state_category = getters.getEquipmentCategory(id)
        return getters.newEquipmentCategory() if not state_category
        category = { ...state_category }
        delete category.equipment
        category
  mutations: {}
  actions:
    createEquipmentCategory: (context, category) ->
      category.id = (await PPE.createEquipmentCategories(category)).data.id
      category.equipment = []
      context.commit('addEquipmentCategory', category)

    updateEquipmentCategory: (context, category) ->
      (await PPE.editEquipmentCategories(category)).data
      category.equipment = context.getters.getEquipmentCategory(category.id).equipment
      context.commit('deleteEquipmentCategory', category)
      context.commit('addEquipmentCategory', category)

    deleteEquipmentCategory: (context, category) ->
      return if category.equipment.length > 0
      await PPE.deleteEquipmentCategories(category.id)
      context.commit('deleteEquipmentCategory', category)
