import PPE from '@/services/PPE'

export default
  state: {}
  getters:
    getAttachment: (state, getters, rootState) =>
      (id) ->
        id = parseInt(id)
        rootState.attachments.find((attachment) => attachment.id == id)
    newAttachment: (state) =>
      () ->
        name: ''
        file: null
        equipment: []
    editAttachment: (state, getters, rootState) =>
      (id) ->
        state_attachment = getters.getAttachment(id)
        return getters.newAttachment() if not state_attachment
        attachment = { ...state_attachment }
        attachment.category = { ...state_attachment.category }
        delete attachment.category.attachments
        attachment.equipment = []
        for state_equipment in state_attachment.equipment
          equipment = { ...state_equipment }
          delete equipment.attachments
          attachment.equipment.push(equipment) 
        attachment
    getAttachmentEquipment: (state, getters, rootState) =>
      (attachment, id) ->
        id = parseInt(id)
        attachment.equipment.find((equipment) => equipment.id == id)
  mutations: {}
  actions:
    createAttachment: (context, attachment) ->
      attachment.id = (await PPE.createAttachments(attachment)).data.id
      context.commit('addAttachment', attachment)
    updateAttachment: (context, attachment) ->
      old_attachment = context.getters.getAttachment(attachment.id)
      (await PPE.editAttachments(attachment)).data
      for equipment in old_attachment.equipment
        if not context.getters.getAttachmentEquipment(attachment, equipment.id)
          await PPE.removeEquipmentAttachments(equipment.id, attachment.id)
      for equipment in attachment.equipment
        if not context.getters.getAttachementEquipment(old_attachment, equipment.id)
          await PPE.addEquipmentAttachments(equipment.id, attachment.id)
      context.commit('deleteAttachment', old_attachment)
      context.commit('addAttachment', attachment)
      for equipment in attachment.equipment
        context.commit('addEquipmentAttachment', equipment, attachment)
    deleteAttachment: (context, attachment) ->
      await PPE.deleteAttachments(attachment.id)
      context.commit('deleteAttachment', attachment)
