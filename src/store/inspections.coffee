import PPE from '@/services/PPE'

export default
  state: {}
  getters:
    getInspection: (state, getters, rootState) =>
      (equipment_id, id) ->
        equipment_id = parseInt(equipment_id)
        id = parseInt(id)
        equipment = getters.getEquipment(equipment_id)
        return undefined if not equipment
        equipment.inspections.find((inspection) => inspection.id == id)
    getInspectionBatchPart: (state, getters, rootState) =>
      (inspection, id) ->
        id = parseInt(id)
        inspection.batch_parts.find((batch_part) => batch_part.id == id)
    newInspection: (state, getters, rootState) =>
      (equipment_id) ->
        equipment_id = parseInt(equipment_id)
        inspection =
          date: new Date().toISOString().substr(0, 10)
          description: ''
          pass: true
          event: ''
          batch_parts: []
          equipment_id: equipment_id
        equipment = getters.getEquipment(equipment_id)
        if equipment
          for equipment_batch_part in equipment.batch_parts when getters.equipmentBatchPartInspectionsPass(equipment_batch_part)
            batch_part = 
              description: ''
              pass: true
              equipment_batch_part: { ...equipment_batch_part }
              equipment_batch_part_id: equipment_batch_part.id
            delete batch_part.equipment_batch_part.equipment
            delete batch_part.equipment_batch_part.inspections
            inspection.batch_parts.push(batch_part)
        inspection
    editInspection: (state, getters, rootState) =>
      (equipment_id, id) ->
        state_inspection = getters.getInspection(equipment_id, id)
        return getters.newInspection(equipment_id) if not state_inspection
        inspection = { ...state_inspection }
        delete inspection.equipment
        inspection.batch_parts = []
        for state_batch_part in state_inspection.batch_parts
          batch_part = { ...state_batch_part }
          delete batch_part.inspection
          batch_part.equipment_batch_part = { ...state_batch_part.equipment_batch_part }
          delete batch_part.equipment_batch_part.inspections
          delete batch_part.equipment_batch_part.equipment
          inspection.batch_parts.push(batch_part)
        inspection
  mutations: {}
  actions:
    createInspection: (context, inspection) ->
      inspection.id = (await PPE.createInspections(inspection.equipment_id, inspection)).data.id
      for batch_part in inspection.batch_parts
        batch_part.id = (await PPE.createInspectionsBatchParts(inspection.equipment_id, inspection.id, batch_part)).data.id
      context.commit('addInspection', inspection)
    updateInspection: (context, inspection) ->
      old_inspection = context.getters.getInspection(inspection.equipment_id, inspection.id)
      (await PPE.editInspections(inspection.equipment_id, inspection)).data
      for batch_part in inspection.batch_parts
        if batch_part.id
          await PPE.editInspectionsBatchParts(inspection.equipment_id, inspection.id, batch_part)
        else
          batch_part.id = (await PPE.createInspectionsBatchParts(inspection.equipment_id, inspection.id, batch_part)).data.id
      context.commit('deleteInspection', old_inspection)
      context.commit('addInspection', inspection)
    deleteInspection: (context, inspection) ->
      await PPE.deleteInspections(inspection.equipment_id, inspection.id)
      context.commit('deleteInspection', inspection)
