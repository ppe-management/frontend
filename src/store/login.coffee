import Api from '@/services/Api'
import PPE from '@/services/PPE'

export default
  state:
    logged: false
  getters:
    logged: (state) => state.logged
  mutations: 
    login: (state) ->
      state.logged = true
    login_from_storage: (state) ->
    logout: (state) ->
      state.logged = false
  actions:
    login: (context, params) ->
      response = await PPE.login(params)
      Api.login(params.user.email, response.data.authentication_token)
      if params.remember
        localStorage.setItem('email', params.user.email)
        localStorage.setItem('token', response.data.authentication_token)
      context.commit('login')
    login_from_storage: (context) ->
      email = localStorage.getItem('email')
      token = localStorage.getItem('token')
      if token
        Api.login(email, token)
        context.commit('login')
    logout: (context) ->
      Api.logout()
      localStorage.removeItem('eeail')
      localStorage.removeItem('token')
      context.commit('logout')
