import PPE from '@/services/PPE'

export default
  state: {}
  getters:
    equipmentInspectionsPass: (state, getters, rootState) =>
      (equipment) ->
        pass = true
        if equipment.batch_parts.length > 0
          pass = false
          pass ||= getters.equipmentBatchPartInspectionsPass(batch_part) for batch_part in equipment.batch_parts
        else if equipment.inspections.length > 0
          pass = equipment.inspections[equipment.inspections.length - 1].pass
        pass
    equipmentBatchPartInspectionsPass: (state, getters, rootState) =>
      (batch_part) ->
        pass = true
        if batch_part.inspections.length > 0
          pass = batch_part.inspections[batch_part.inspections.length - 1].pass
        pass
    equipmentInspectionOverdue: (state, getters, rootState) =>
      (equipment) ->
        overdue = false
        if !getters.equipmentInspectionsPass(equipment)
          overdue = false
        else
          today = new Date
          if equipment.inspections.length == 0
            date = equipment.manufacturing_date
          else
            date = equipment.inspections[equipment.inspections.length - 1].date
          date = new Date(date)
          overdue = today.getTime() - date.getTime() > 1 * 335 * 24 * 3600 * 1000 # number of ms in 1 year - 30 days
        overdue
    equipmentUseOverdue: (state, getters, rootState) =>
      (equipment) ->
        overdue = false
        if getters.equipmentInspectionsPass(equipment) and equipment.end_of_use_date
          today = new Date
          date = new Date(equipment.end_of_use_date)
          overdue = today > date
        overdue
    getEquipment: (state, getters, rootState) =>
      (id) ->
        id = parseInt(id)
        rootState.equipment.find((equipment) => equipment.id == id)
    getEquipmentBatchPart: (state, getters, rootState) =>
      (equipment, id) ->
        id = parseInt(id)
        equipment.batch_parts.find((batch_part) => batch_part.id == id)
    getEquipmentAttachment: (state, getters, rootState) =>
      (equipment, id) ->
        id = parseInt(id)
        equipment.attachments.find((attachment) => attachment.id == id)
    newEquipment: (state) =>
      () ->
        identifier: ''
        make: ''
        model: ''
        serial: ''
        category_id: null
        description: ''
        batch: false
        batch_parts: []
        inspections: []
        attachments: []
        manufacturing_date: new Date().toISOString().substr(0, 10)
        purchase_date: new Date().toISOString().substr(0, 10)
        first_use_date: new Date().toISOString().substr(0, 10)
        end_of_use_date: new Date().toISOString().substr(0, 10)
    newEquipmentBatchPart: (state) =>
      (equipment) ->
        batch_part =
          identifier: ''
          serial: ''
          description: ''
          inspections: []
        if equipment.batch_parts.length > 0
          identifier = equipment.batch_parts[equipment.batch_parts.length - 1].identifier
          batch_part.identifier = m[1] + (parseInt(m[2]) + 1) if (m = identifier.match(/(.*[^\d]+)([\d]+)$/))
        else if equipment.identifier
          batch_part.identifier = equipment.identifier + '-1'
        batch_part
    editEquipment: (state, getters, rootState) =>
      (id) ->
        state_equipment = getters.getEquipment(id)
        return getters.newEquipment() if not state_equipment
        equipment = { ...state_equipment }
        delete equipment.inspections
        equipment.category = { ...state_equipment.category }
        delete equipment.category.equipment
        equipment.batch_parts = []
        for state_batch_part in state_equipment.batch_parts
          batch_part = { ...state_batch_part }
          batch_part.form_id = batch_part.id
          delete batch_part.equipment
          delete batch_part.inspections
          equipment.batch_parts.push(batch_part) 
        equipment.attachments = []
        for state_attachment in state_equipment.attachments
          attachment = { ...state_attachment }
          delete attachment.equipment
          attachment.category = { ...state_attachment.category }
          delete attachment.category.attachments
          equipment.attachments.push(attachment)
        equipment
  mutations: {}
  actions:
    createEquipment: (context, equipment) ->
      equipment.id = (await PPE.createEquipment(equipment)).data.id
      for batch_part in equipment.batch_parts
        batch_part.id = (await PPE.createEquipmentBatchParts(equipment.id, batch_part)).data.id
      context.commit('addEquipment', equipment)
    updateEquipment: (context, equipment) ->
      for attachment in equipment.attachments
        delete attachment.equipment
      old_equipment = context.getters.getEquipment(equipment.id)
      (await PPE.editEquipment(equipment)).data
      for batch_part in equipment.batch_parts
        if batch_part.id
          await PPE.editEquipmentBatchParts(equipment.id, batch_part)
        else
          batch_part.id = (await PPE.createEquipmentBatchParts(equipment.id, batch_part)).data.id
      for batch_part in old_equipment.batch_parts
        if not context.getters.getEquipmentBatchPart(equipment, batch_part.id)
          await PPE.deleteEquipmentBatchParts(equipment.id, batch_part.id)
          for batch_part_inspection in batch_part.inspections
            batch_part_inspection.inspection.batch_parts.splice(batch_part_inspection.inspection.batch_parts.indexOf(batch_part_inspection), 1)
      for attachment in old_equipment.attachments
        if not context.getters.getEquipmentAttachment(equipment, attachment.id)
          await PPE.removeEquipmentAttachments(equipment.id, attachment.id)
      for attachment in equipment.attachments
        if not context.getters.getEquipmentAttachment(old_equipment, attachment.id)
          await PPE.addEquipmentAttachments(equipment.id, attachment.id)
      attachments = equipment.attachments
      context.commit('deleteEquipment', old_equipment)
      context.commit('addEquipment', equipment)
      context.commit('addInspection', inspection) for inspection in old_equipment.inspections
      context.commit('addEquipmentAttachment', {equipment, attachment_id: attachment.id}) for attachment in attachments
    deleteEquipment: (context, equipment) ->
      await PPE.deleteEquipment(equipment.id)
      context.commit('deleteEquipment', equipment)
