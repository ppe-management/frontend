import Vue from 'vue'
import Vuex from 'vuex'
import Api from '@/services/Api'
import PPE from '@/services/PPE'
import EquipmentCategoriesStore from '@/store/equipment_categories'
import EquipmentStore from '@/store/equipment'
import InspectionsStore from '@/store/inspections'
import AttachmentsCategoriesStore from '@/store/attachments_categories'
import AttachmentsStore from '@/store/attachments'
import LoginStore from '@/store/login'

Vue.use(Vuex)

export default new Vuex.Store(
  state:
    equipment: []
    equipment_categories: []
    attachments: []
    attachments_categories: []
  getters:
    equipment: (state) => state.equipment
    equipment_categories: (state) => state.equipment_categories
    attachments: (state) => state.attachments
    attachments_categories: (state) => state.attachments_categories
  mutations:
    addEquipmentCategory: (state, category) ->
      state.equipment_categories.push(category)
      Vue.set(category, 'equipment', [])

    deleteEquipmentCategory: (state, category) ->
      state_category = state.equipment_categories.find((category_item) => category_item.id == category.id)
      state.equipment_categories.splice(state.equipment_categories.indexOf(state_category), 1)

    addEquipment: (state, equipment) ->
      state.equipment.push(equipment)

      equipment.category = state.equipment_categories.find((category) => category.id == equipment.category_id)
      equipment.category.equipment.push(equipment) if equipment.category
      for batch_part in equipment.batch_parts
        Vue.set(batch_part, 'equipment', equipment)
        Vue.set(batch_part, 'inspections', [])
      Vue.set(equipment, 'inspections', [])
      Vue.set(equipment, 'attachments', [])

    deleteEquipment: (state, equipment) ->
      state_equipment = state.equipment.find((equipment_item) => equipment_item.id == equipment.id)
      state_equipment.category.equipment.splice(state_equipment.category.equipment.indexOf(state_equipment), 1) if state_equipment.category
      for attachment in state_equipment.attachments
        attachment.equipment.splice(attachment.equipment.indexOf(attachment), 1)
      state.equipment.splice(state.equipment.indexOf(state_equipment), 1)

    addInspection: (state, inspection) ->
      equipment = state.equipment.find((equipment_item) => equipment_item.id == inspection.equipment_id)
      equipment.inspections.push(inspection)

      equipment = state.equipment.find((equipment_item) => equipment_item.id == inspection.equipment_id)
      Vue.set(inspection, 'equipment', equipment)
      for batch_part in inspection.batch_parts
        equipment_batch_part = equipment.batch_parts.find((batch_part_item) => batch_part_item.id == batch_part.equipment_batch_part_id)
        Vue.set(batch_part, 'equipment_batch_part', equipment_batch_part)
        Vue.set(batch_part, 'inspection', inspection)
        equipment_batch_part.inspections.push(batch_part)

    deleteInspection: (state, inspection) ->
      equipment = state.equipment.find((equipment_item) => equipment_item.id == inspection.equipment_id)
      state_inspection = equipment.inspections.find((inspection_item) => inspection_item.id == inspection.id)
      equipment.inspections.splice(equipment.inspections.indexOf(state_inspection), 1)
      for batch_part in state_inspection.batch_parts
        equipment_batch_part = equipment.batch_parts.find((batch_part_item) => batch_part_item.id == batch_part.equipment_batch_part_id)
        equipment_batch_part.inspections.splice(equipment_batch_part.inspections.indexOf(batch_part), 1)

    addAttachmentsCategory: (state, category) ->
      state.attachments_categories.push(category)
      Vue.set(category, 'attachments', [])

    deleteAttachmentsCategory: (state, category) ->
      state_category = state.attachments_categories.find((category_item) => category_item.id == category.id)
      state.attachments_categories.splice(state.attachments_categories.indexOf(state_category), 1)

    addAttachment: (state, attachment) ->
      attachment.file.url = Api.baseURL + attachment.file.url
      state.attachments.push(attachment)

      attachment.category = state.attachments_categories.find((category) => category.id == attachment.category_id)
      attachment.category.attachments.push(attachment) if attachment.category
      Vue.set(attachment, 'equipment', [])

    deleteAttachment: (state, attachment) ->
      state_attachment = state.attachments.find((attachment_item) => attachment_item.id == attachment.id)
      state_attachment.category.attachments.splice(state_attachment.category.attachments.indexOf(state_attachment), 1) if state_attachment.category
      for equipment in state_attachment.equipment
        equipment.attachments.splice(equipment.attachments.indexOf(state_attachment), 1)
      state.attachments.splice(state.attachments.indexOf(state_attachment), 1)

    addEquipmentAttachment: (state, {equipment, attachment_id}) ->
      attachment = state.attachments.find((attachment_item) => attachment_item.id == attachment_id)
      attachment.equipment.push(equipment)
      equipment.attachments.push(attachment)

  actions:
    list: (context) ->
      attachments_categories = (await PPE.listAttachmentsCategories()).data
      for category in attachments_categories
        context.commit('addAttachmentsCategory', category)

      for attachment in (await PPE.listAttachments()).data
        context.commit('addAttachment', attachment)

      equipment_categories = (await PPE.listEquipmentCategories()).data
      for category in equipment_categories
        context.commit('addEquipmentCategory', category)

      for equipment in (await PPE.listEquipment()).data
        equipment.batch_parts = (await PPE.listEquipmentBatchParts(equipment.id)).data
        context.commit('addEquipment', equipment)
        inspections = (await PPE.listInspections(equipment.id)).data
        for inspection in inspections
          inspection.batch_parts = (await PPE.listInspectionsBatchParts(equipment.id, inspection.id)).data 
          context.commit('addInspection', inspection)
        for attachment in (await PPE.listEquipmentAttachments(equipment.id)).data
          context.commit('addEquipmentAttachment', {equipment, attachment_id: attachment.id})

  modules: {
    EquipmentCategoriesStore
    EquipmentStore
    InspectionsStore
    AttachmentsCategoriesStore
    AttachmentsStore
    LoginStore
  }
)
