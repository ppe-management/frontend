import Vue from 'vue'
import VueI18n from 'vue-i18n'
import frValidationMessages from 'vee-validate/dist/locale/fr'
import enValidationMessages from 'vee-validate/dist/locale/en'

Vue.use(VueI18n)

function loadLocaleMessages () {
  const validationMessages = {
    fr: frValidationMessages,
    en: enValidationMessages
  }
  const locales = require.context('./locales', true, /[A-Za-z0-9-_,\s]+\.json$/i)
  const messages = {}
  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i)
    if (matched && matched.length > 1) {
      const locale = matched[1]
      messages[locale] = locales(key)
      if (validationMessages[locale])
        if (!messages[locale].validations)
          messages[locale].validations = {}
        Object.assign(messages[locale].validations, validationMessages[locale].messages)
    }
  })
  return messages
}

export default new VueI18n({
  locale: process.env.VUE_APP_I18N_LOCALE || 'en',
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'en',
  messages: loadLocaleMessages()
})
