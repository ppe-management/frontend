import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Logout from '../views/Logout.vue'
import EquipmentIndex from '../views/Equipment/Index.vue'
import EquipmentExport from '../views/Equipment/Export.vue'
import EquipmentCreate from '../views/Equipment/Create.vue'
import EquipmentEdit from '../views/Equipment/Edit.vue'
import InspectionsCreate from '../views/Inspections/Create.vue'
import InspectionsEdit from '../views/Inspections/Edit.vue'
import EquipmentCategoriesIndex from '../views/EquipmentCategories/Index.vue'
import EquipmentCategoriesCreate from '../views/EquipmentCategories/Create.vue'
import EquipmentCategoriesEdit from '../views/EquipmentCategories/Edit.vue'
import AttachmentsCategoriesIndex from '../views/AttachmentsCategories/Index.vue'
import AttachmentsCategoriesCreate from '../views/AttachmentsCategories/Create.vue'
import AttachmentsCategoriesEdit from '../views/AttachmentsCategories/Edit.vue'
import AttachmentsIndex from '../views/Attachments/Index.vue'
import AttachmentsCreate from '../views/Attachments/Create.vue'
import AttachmentsEdit from '../views/Attachments/Edit.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: {
      name: 'equipment'
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/logout',
    name: 'logout',
    component: Logout
  },
  {
    path: '/equipment',
    name: 'equipment',
    component: EquipmentIndex
  },
  {
    path: '/equipment.zip',
    name: 'equipment-export',
    component: EquipmentExport
  },
  {
    path: '/equipment/create',
    name: 'equipment-create',
    component: EquipmentCreate
  },
  {
    path: '/equipment/:id/edit',
    name: 'equipment-edit',
    component: EquipmentEdit
  },
  {
    path: '/equipment/:equipment_id/inspections/create',
    name: 'inspections-create',
    component: InspectionsCreate
  },
  {
    path: '/equipment/:equipment_id/inspections/:id/edit',
    name: 'inspections-edit',
    component: InspectionsEdit
  },
  {
    path: '/equipment/categories',
    name: 'equipment-categories',
    component: EquipmentCategoriesIndex
  },
  {
    path: '/equipment/categories/create',
    name: 'equipment-categories-create',
    component: EquipmentCategoriesCreate
  },
  {
    path: '/equipment/categories/edit/:id',
    name: 'equipment-categories-edit',
    component: EquipmentCategoriesEdit
  },
  {
    path: '/attachments/categories',
    name: 'attachments-categories',
    component: AttachmentsCategoriesIndex
  },
  {
    path: '/attachments/categories/create',
    name: 'attachments-categories-create',
    component: AttachmentsCategoriesCreate
  },
  {
    path: '/attachments/categories/edit/:id',
    name: 'attachments-categories-edit',
    component: AttachmentsCategoriesEdit
  },
  {
    path: '/attachments',
    name: 'attachments',
    component: AttachmentsIndex
  },
  {
    path: '/attachments/create',
    name: 'attachments-create',
    component: AttachmentsCreate
  },
  {
    path: '/attachments/:id/edit',
    name: 'attachments-edit',
    component: AttachmentsEdit
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
